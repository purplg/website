#+TITLE: Self-hosting
#+DATE: <2024-04-27 Sat>


/You don't need a lab/





Ben Whitley
[[https://purplg.dev/][purplg.dev]]

* Why is self-host?

- Big service providers can and have lost your data.

- Requiring an internet connection to turn on your light while you're home is asinine.

- Privacy, of course.

* Is it expensive?

No! But it can be if you like.

My setup involves a $10/mo and $5/mo Linode VPS. One hosts my own Matrix (Synapse) server and an IRC bouncer and the other is other random small services like an always-online *Syncthing* peer.

* Cloud Alternatives

** IOT Hub - Home Assistant

#+BEGIN_QUOTE
Home Assistant is free and open-source software for home automation, designed to be an Internet of things (IoT) ecosystem-independent integration platform and central control system for smart home devices, with a focus on local control and privacy.
https://en.wikipedia.org/wiki/Home_Assistant
#+END_QUOTE

If a /smart/ device requires an internet connection, then it isn't /smart/. It's dumb with the smarts off-loaded.

Home Assistant has integrations with MANY IOT devices. It enables you to have a smart, connected home while begin offline and local.

Recently, I purchased a smart thermostat (Venstar T7850 for those interested) with a local API feature and it works great -- so long as you install it correctly...

*** Home Automation Hardware

- [[https://cloudfree.shop/][cloudfree.shop]]

- ESP32 (DIY)

- Raspberry PI

- Random Appliances. Lots of Home Assistant integrations (even my toothbrush)
** Offline cloud-connectivity

This seems contradictory.

What I'm describing here is being able to access your *personal* cloud from the internet without exposing your applications or devices directly to the internet.

The way I do this is with a VPN, of course. Specifically, I use Wireguard, but I'm sure something like ZeroTier would work just as well. My phone, laptop, and even Linode VPS's, all have an always-on VPN connection back into my home network so I can always access my any of my services no matter where I am.

*** Travel

Setting up this personal VPN and cloud has the additional benefit of being able to extend your LAN where-ever you go, like in a hotel. A travel router can help with this.

- [[https://www.gl-inet.com/][GL-iNet]] travel routers are great.

- They run *OpenWRT* with a fancy UI but still allow you to access the normal LuCI (web) interface or ssh in and configure it by hand.

- Mine is older and not available anymore.

**** Setup

Configure your travel router to have the same name (SSID) and password as your home LAN.

Set it up as a VPN peer to your home LAN (site-to-site VPN).

Then your phone and other WiFi devices will automatically connect to your travel routers' WiFi as if it were your home WiFI.

You only need to connect your travel router to the hotel WiFi.

All your devices will automagically have internet access AND personal cloud access as if you were home.

Perfect for your devices without a web browser to get the captive portals.

** Chat - Matrix!

#+BEGIN_QUOTE
Matrix is an open standard for interoperable, decentralised, real-time communication over IP. It can be used to power Instant Messaging, VoIP/WebRTC signalling, Internet of Things communication - or anywhere you need a standard HTTP API for publishing and subscribing to data whilst tracking the conversation history.

[[https://matrix.org/docs/older/faq/]]
#+END_QUOTE

But Matrix isn't /just/ another chat platform, it's an open and hackable platform! It supports many bridges to other chat platforms, too, mainly developed by [[https://www.beeper.com/][Beeper]].

I bridge my personal Matrix server to Telegram so my Telegram friends can't even tell.

Clients are still a little rough around the edges though, but the protocol is really promising so far.

You can setup your own server to not federate with other servers so that you can only chat with your friends and family, if you want. I plan on doing this for my family once the clients are in better shape.
** Android - GrapheneOS

Okay fine.. it's still Android. But the main alternative here is to /Google's/ Android.

Ironically, they only support Pixel devices.

And GrapheneOS isn't just another rom, trust me, I've tried a lot of alternative Android ROMs over the years. GrapheneOS is really polished. They focus on on security mostly, but privacy kinda goes hand-in-hand with security.

My favorite feature is being able to run *Google Play Services* as a user application and out-of-the-box has no permissions. You have you specifically enable each permission you want to give it.

Alternatively, you can move *Google Play Services* to just the work profile. In this configuration, you can install the apps that need Google Play Services into your work profile and just turn them on when you need them. Think of the battery life!

** Password Management

Use a password manager! If you're here, I'm sure I don't have to tell you that though.

*** Pass

https://www.passwordstore.org/

If you're a massive nerd like me, you might like using something unix-like, like *pass*. *Pass* simple utility that combines other tools, like *gpg*, *git*, and your file system, to organize your secrets.

It doesn't store only passwords. It can store anything. I've used it to store gift card numbers, account ID's, and much more.

There is even an [[https://github.com/android-password-store/Android-Password-Store][Android client]].

I've been using it since 2015 and don't see myself ever switching from it.

*** Bitwarden / Vaultwarden

There's also the well-known *Bitwarden*. I've never actually used it, so I'd love to hear from you.

*Bitwarden* is probably better for setting up for friends and family. It has a much more user-friendly interface.

*Vaultwarden* is an alternative server implementation that is supposed to be much lighterweight. Apparently the official one is a beast.

Both seem to require an email address to signup even if you host it yourself, which implies to me that it requires an official account. This is the main reason I haven't considered using it.

** Notes - Emacs (of course)

Note taking systems are probably *the* most subjective tool out there. They're how you organize your brain, life, tasks...everything. You gotta find the tool the fits the shape of your brain.

Anyone who knows me knows I love Emacs. I couldn't go an entire presentation without mentioning it, of course.

So I use org-mode, but I won't go into too much detail about that here since I talked about it in my last talk.

But what you are looking at is Emacs and org-mode. I brain-dumped this notes and then just turned them into a presentation.

For me, the best tool is the one you're going to use. And since I find Emacs fun, making my notes fun by using org-mode has been very effective.

So try to find a tool that's fun!

There's also tools like [[https://obsidian.md/][Obsidian]] and [[https://joplinapp.org/][Joplin]], both of which store your notes in an open an accessible format so you can sync them yourself. They also offer a paid service to manage the syncing for you /if you want/. Self-hosting is about choice.

** Finances - ledger

#+BEGIN_SRC ledger
2024-04-27 * Some internet company
    Liabilities:CreditCard           $-20.00
    Expenses:Bill:Internet            $20.00
#+END_SRC

*Ledger* falls in a category of software called [[https://plaintextaccounting.org/][Plaintext Accounting]]. It is a double-entry accounting system entirely in, you guessed it, plain text!

Using *ledger* is such a breath of fresh air for managing money. If you make a mistake.. you just.. change the text. No complicated software that may-or-may-not work when you need it.

Every time you run ledger, it "renders" your ledger file. So there's no previous state between runs you have to balance and fight with.

It's a essentially just an extremely flexible markup format. It can be as complicated or as simple as you need it. If you just want to track the credits and debits of a single account, sure do that.

But if you want to track your 401k or stock trades, or whatever other fancy-schmancy finance words (not me), it can do that with an incredible amount of power.

[[file:example.ledger][Example]]

There's also ~hledger~, which provides alternative interfaces, like a CLI and web interface, but uses more-or-less the same compatible syntax as *ledger*. I've been using *hledger* myself.

** File sync - Syncthing

Most of the services wouldn't be that useful if you weren't able to synchronize the state of them between devices.

And I'm sure many of you have heard, or actively use, *Syncthing*.

It's a way to synchronize a directory between multiple devices. I use it for my notes, ledger, agenda, random files, and even game save files for emulators.

** Honorable mentions

*** SteamDeck

So the *SteamDeck* doesn't directly fit into the self-hosting category, but it's open and hackable nature allows you to take advantage of some of your future self-hosting endeavors.

Since the *SteamDeck* is the ultimate, handheld, gaming and emulation device, I use *Syncthing* to synchronize my game saves between my devices.

In fact, I played all of *Tears of the Kingdom* on my *SteamDeck*. -- after legally purchasing and dumping the ROM from my gen1 Switch, yes really :)

But sometimes I'd streamed it from my desktop instead of playing natively on the 'Deck, so having a way to keep my save files in sync was invaluable.

* What do you use?

There's a lot I didn't talk about here, so what do you use? Did I miss anything awesome? What is your dream setup?
